#!/usr/bin/make -f
# -*- makefile -*-

# FR the include below loads various mk files:
#
include /usr/share/dpkg/default.mk
#
# include $(dpkg_datadir)/architecture.mk
# include $(dpkg_datadir)/buildflags.mk
# include $(dpkg_datadir)/pkg-info.mk
# include $(dpkg_datadir)/vendor.mk


export DH_VERBOSE=1
export DH_OPTIONS=-v

CMAKE=/usr/bin/cmake

export DEB_HOST_MULTIARCH ?= $(shell dpkg-architecture -qDEB_HOST_MULTIARCH)

BUILD_DIR = $(CURDIR)/obj-${DEB_HOST_MULTIARCH}
DEBIAN_DIR = $(CURDIR)/debian

# Path to the tools subdirectory in the top source dir to build the
# documentation.
export PATH := $(BUILD_DIR)/bin:$(CURDIR)/tools:$(PATH)
export LD_LIBRARY_PATH := $(BUILD_DIR)/lib/$(DEB_HOST_MULTIARCH):$(LD_LIBRARY_PATH)

export LIB_VERSION = 2.5.0
export LIB_SOVERSION = 2

# Hardening stuff, see http://wiki.debian.org/Hardening
export DEB_BUILD_MAINT_OPTIONS = hardening=+all
DPKG_EXPORT_BUILDFLAGS = 1

# Fix bug #964315 Adrian Bunk <bunk@debian.org>
ifneq (,$(filter $(DEB_HOST_ARCH), mipsel))
  export DEB_CXXFLAGS_MAINT_APPEND += -g1
endif


%:
	dh $@ --buildsystem=cmake --no-parallel

override_dh_clean:
	@echo "entering the override_dh_clean target"

	# Remove the generated xpm icon files:
	rm -f debian/TOPPAS.xpm
	rm -f debian/TOPPView.xpm
	
	# Remove the generated topp.1 man page:
	rm -f debian/topp.1

	dh_clean


override_dh_auto_configure:

# -DHAS_XSERVER=OFF to let the doc building process that no 
#  XWindow server is available.
	dh_auto_configure -- \
		-DDEBIAN_BUILD=ON \
		-DHAS_XSERVER=OFF \
		-DCMAKE_FIND_LIBRARY_SUFFIXES=".so" \
		-DCMAKE_SKIP_RPATH=ON \
		-DCMAKE_INSTALL_PREFIX="/usr" \
		-DOPENMS_CONTRIB_LIBS="/usr/lib;/usr/lib/$(DEB_HOST_MULTIARCH)" \
		-DCMAKE_BUILD_TYPE=release \
		-DBOOST_USE_STATIC=OFF \
		-DENABLE_STYLE_TESTING=OFF \
		-DENABLE_TOPP_TESTING=OFF \
		-DENABLE_CLASS_TESTING=OFF \
		-DENABLE_PIPELINE_TESTING=OFF \
 		-DENABLE_TUTORIALS=ON
 	
	# Prepare the xpm-formatted pixmaps for the Debian menu system.
	# Setting imagemagick to Build-Depends, for use of convert.
	convert src/openms_gui/source/VISUAL/ICONS/TOPPAS.png -resize 32x32 debian/TOPPAS.xpm
	convert src/openms_gui/source/VISUAL/ICONS/TOPPView.png -resize 32x32 debian/TOPPView.xpm
	
	
override_dh_auto_build:

	# Build the libs and executable binaries
	# Build the documentation, which needs a running XWindow server. We
	# thus use the doc_minimal target.
	# 
	# ATTENTION, as of 20200514, the pdfLatex compilation fails with error
	# ! LaTeX Error: Command `\Bbbk' already defined.
	# This is why we commented out the doc_tutorials target below.
	# See also the override_dh_auto_configure target above, that disables
	# that specific doc_tutorials target.

	VERBOSE=1 \
					dh_auto_build -- \
					OpenMS GUI TOPP UTILS

	# We need to tell the documenter program where the shared data are
	# in the source tree: share/OpenMS. That is not the install directory!
	# Also tell the CMake build system that we'll build the docs without
	# access to a XWindow server.
	VERBOSE=1 \
					OPENMS_DATA_PATH=$(CURDIR)/share/OpenMS \
					HAS_XSERVER=OFF \
					dh_auto_build -- doc_minimal
	
	# Run the test suite
	# VERBOSE=1 $(MAKE) -C $(BUILD_DIR) test

	# Prepare the topp.1 man page with all the one-line description got
	# from calling individually each program in $(BUILD_DIR)/bin with
	# --help switch. The newly built libs need be in the path so that the
	# binaries can execute. Same for the shared data path, because the
	# binaries need it to run. Note the calling in sh ./ with the script
	# shell being itself executable, otherwise git-buildpackage would
	# fail.
	cd $(DEBIAN_DIR) && \
		PATH=$(BUILD_DIR)/bin:$(CURDIR)/tools:$(PATH) \
		OPENMS_DATA_PATH=$(CURDIR)/share/OpenMS/ \
		sh ./binaries-extract-one-line-man-desc.sh $(BUILD_DIR)/bin $(DEBIAN_DIR)

	#VERBOSE=1 LD_LIBRARY_PATH=$(BUILD_DIR)/lib/:$(LD_LIBRARY_PATH)\
		#PATH=$(BUILD_DIR)/bin:$(CURDIR)/tools:$(PATH)\
		#OPENMS_DATA_PATH=$(CURDIR)/share/OpenMS/ \
		#cd $(BUILD_DIR)/doc/doxygen && doxygen Doxyfile
	
	# There are two files that we do not want to ship because
	# they exist in their own Debian packages.
	
	rm -f $(BUILD_DIR)/usr/share/doc/html/jquery.js


override_dh_auto_test:
	

override_dh_auto_install:

	VERBOSE=1 \
		OPENMS_DATA_PATH=$(CURDIR)/share/OpenMS/ \
		dh_auto_install

